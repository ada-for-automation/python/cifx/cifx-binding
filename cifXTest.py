#!/usr/bin/env python3

"""
 This Python script implements a simple test of Hilscher cifX binding
"""

from cffi_cifx_linux import ffi as ffilinux, lib as liblinux
from cffi_cifx import ffi, lib

from cifX_misc import \
    int8_to_hex, int32_to_hex, \
    Linux_cifXGetDriverVersion, Log_Error, cifXGetDriverVersion

import kbhit, time

def main():
    """
    cifXTest Main
    """

    print("=======================================\n"
          "             cifXTest Main !           \n"
          "=======================================\n")

    kbhit.init();
    running = True;

    CIFX_DRIVER_INIT_AUTOSCAN = 1

    OldResult = 0
    Result = 0
    info = {}

    cifX_Init_Done = False
    cifX_Driver_Open_Done = False
    cifX_Channel_Open_Done = False

    pInit_params = ffilinux.new("struct CIFX_LINUX_INIT*")

    pInit_params.init_options = CIFX_DRIVER_INIT_AUTOSCAN

    print("Initialising Linux Driver")
    Result = liblinux.cifXDriverInit(pInit_params)

    if Result != 0 :
        print("Error cifXDriverInit :", int32_to_hex(Result))
    else:
        cifX_Init_Done = True

    if cifX_Init_Done:
        Linux_cifXGetDriverVersion(info)

    if cifX_Init_Done:
        phDriver = ffi.new("CIFXHANDLE*")

        print("Opening Driver")
        Result = lib.xDriverOpen(phDriver)

        if Result != OldResult :
            OldResult = Result
            Log_Error("xDriverOpen", Result)

        if Result == 0 :
            cifX_Driver_Open_Done = True

    if cifX_Driver_Open_Done:
        cifXGetDriverVersion(phDriver[0], info)

    if cifX_Driver_Open_Done:
        phChannel = ffi.new("CIFXHANDLE*")
        pBoard = ffi.new("char[]", b"cifX0")

        print("Opening Channel")
        Result = lib.xChannelOpen(phDriver[0], pBoard, 0, phChannel)

        if Result != OldResult :
            OldResult = Result
            Log_Error("xChannelOpen", Result)

        if Result == 0 :
            cifX_Channel_Open_Done = True

    print("Information:\n", info)

    if cifX_Channel_Open_Done:
        print("===================================\n"
              "     IO Loop : q to terminate !\n"
              "===================================\n")

        pInData = ffi.new("uint8_t[20]")
        pOutData = ffi.new("uint8_t[20]")
        print("sizeof(pInData) =", ffi.sizeof(pInData))

        while running:

            if kbhit.kbhit():
                ch = kbhit.getch();
                if 'q' == ch:
                    running = False;
                    print("User interrupt !")

            Result = \
              lib.xChannelIORead(phChannel[0],        # Channel Handle
                                 0,                   # AreaNumber
                                 0,                   # Offset
                                 ffi.sizeof(pInData), # DataLen
                                 pInData,             # Data
                                 10)                  # Timeout

            if Result != OldResult :
                OldResult = Result
                Log_Error("xChannelIORead", Result)

            if Result == 0 :

                pOutData[0:ffi.sizeof(pOutData)] = \
                                                pInData[0:ffi.sizeof(pOutData)]

                Result = \
                  lib.xChannelIOWrite(phChannel[0],         # Channel Handle
                                      0,                    # AreaNumber
                                      0,                    # Offset
                                      ffi.sizeof(pOutData), # DataLen
                                      pOutData,             # Data
                                      10)                   # Timeout

                if Result != OldResult :
                    OldResult = Result
                    Log_Error("xChannelIOWrite", Result)

            print("Zzzzh...", int8_to_hex(pInData[0]), int8_to_hex(pInData[1]))
            time.sleep(1.05)

    if cifX_Channel_Open_Done:
        print("Closing Channel")
        Result = lib.xChannelClose(phChannel[0])
        cifX_Channel_Open_Done = False

        if Result != OldResult :
            OldResult = Result
            Log_Error("xChannelClose", Result)

    if cifX_Driver_Open_Done:
        print("Closing Driver")
        Result = lib.xDriverClose(phDriver[0])
        cifX_Driver_Open_Done = False

        if Result != OldResult :
            OldResult = Result
            Log_Error("xDriverClose", Result)

    if cifX_Init_Done:
        print("Linux Driver DeInit")
        liblinux.cifXDriverDeinit()
        cifX_Init_Done = False

    kbhit.restore();

if __name__ == "__main__":
    main()

