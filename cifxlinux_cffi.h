
/*
From cifxlinux.h :
----------------
touch cifxlinux_cffi.h
and import definitions
*/

#define CIFX_DRIVER_INIT_NOSCAN     0  /*!< Don't automatically scan and add found devices                 */
#define CIFX_DRIVER_INIT_AUTOSCAN   1  /*!< Scan automatically for devices and add them to toolkit control */
#define CIFX_DRIVER_INIT_CARDNUMBER 2  /*!< Initialize specific card                                       */

/*****************************************************************************/
/*! Driver initialization structure                                          */
/*****************************************************************************/
struct CIFX_LINUX_INIT
{
  int                   init_options;   /*!< see CIFX_DRIVER_INIT_XXX defines */

  const char*           base_dir;       /*!< base directory for device configuration */
  unsigned long         poll_interval;  /*!< Poll interval in ms for non-irq cards   */
  int                   poll_priority;  /*!< Poll thread priority                    */
  unsigned long         trace_level;    /*!< see TRACE_LVL_XXX defines               */
  int                   user_card_cnt;  /*!< Number of user defined cards            */
  struct CIFX_DEVICE_T* user_cards;     /*!< Pointer to Array of user cards (must be user_card_cnt long) */

  int                   iCardNumber;
  int                   fEnableCardLocking;
  int                   poll_StackSize;   /*!< Stack size of polling thread            */
  int                   poll_schedpolicy; /*!< Schedule policy of poll thread          */
  FILE*                 logfd;
};

int32_t cifXDriverInit(const struct CIFX_LINUX_INIT* init_params);
void    cifXDriverDeinit();
int32_t cifXGetDriverVersion(uint32_t ulSize, char* szVersion);


