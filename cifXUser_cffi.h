
/*
From cifXUser.h :
----------------
touch cifXUser_cffi.h
and import definitions
*/

/* ------------------------------------------------------------------------------------ */
/*  global definitions                                                                  */
/* ------------------------------------------------------------------------------------ */
typedef void* CIFXHANDLE;

/*****************************************************************************/
/*! Structure definitions                                                    */
/*****************************************************************************/
typedef __CIFx_PACKED_PRE struct DRIVER_INFORMATIONtag
{
  char       abDriverVersion[32];                          /*!< Driver version                 */
  uint32_t   ulBoardCnt;                                   /*!< Number of available Boards     */
} __CIFx_PACKED_POST DRIVER_INFORMATION;

/***************************************************************************
* API Functions
***************************************************************************/

/* Global driver functions */
int32_t APIENTRY xDriverOpen                 ( CIFXHANDLE* phDriver);
int32_t APIENTRY xDriverClose                ( CIFXHANDLE  hDriver);

int32_t APIENTRY xDriverGetInformation       ( CIFXHANDLE  hDriver, uint32_t ulSize, void* pvDriverInfo);
int32_t APIENTRY xDriverGetErrorDescription  ( int32_t     lError,  char* szBuffer, uint32_t ulBufferLen);

/* Channel depending functions */
int32_t APIENTRY xChannelOpen                ( CIFXHANDLE  hDriver,  char* szBoard, uint32_t ulChannel, CIFXHANDLE* phChannel);
int32_t APIENTRY xChannelClose               ( CIFXHANDLE  hChannel);

int32_t APIENTRY xChannelIORead              ( CIFXHANDLE  hChannel, uint32_t ulAreaNumber, uint32_t ulOffset,     uint32_t ulDataLen, void* pvData, uint32_t ulTimeout);
int32_t APIENTRY xChannelIOWrite             ( CIFXHANDLE  hChannel, uint32_t ulAreaNumber, uint32_t ulOffset,     uint32_t ulDataLen, void* pvData, uint32_t ulTimeout);

