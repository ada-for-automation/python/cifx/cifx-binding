"""
    --------------------Hilscher cifX Python binding---------------
    Task definitions for invoke command line utility for python bindings
"""

import cffi
import invoke
import pathlib
import sys
import os
import shutil
import re
import glob

on_win = sys.platform.startswith("win")

def print_banner(msg):
    print("========================================================")
    print("= invoke : {} ".format(msg))

@invoke.task
def clean(c):
    """
    Remove any built objects.
    """

    print_banner("Running clean")

    for file_pattern in (
        "*.o",
        "*.so",
    ):
        for file in glob.glob(file_pattern):
            os.remove(file)

    for dir_pattern in "Release":
        for dir in glob.glob(dir_pattern):
            shutil.rmtree(dir)

    print("invoke : Done !")

@invoke.task()
def build_cffi_cifx_linux(c):
    """
    Build the CFFI Hilscher cifX Linux Python bindings.
    """

    print_banner("Building CFFI Hilscher cifX Linux Module")

    ffi = cffi.FFI()

    this_dir = pathlib.Path().resolve()
    h_file_name = this_dir / "cifxlinux_cffi.h"

    with open(h_file_name) as h_file:
        # cffi does not support directives yet,
        # so we need to remove them
        lns = h_file.read().splitlines()
        flt = filter(lambda ln: not re.match(r" *#", ln), lns)
        ffi.cdef(str("\n").join(flt))

        ffi.set_source(
            "cffi_cifx_linux",
            """
            #include "cifxlinux.h"
            """,
            include_dirs = ["/usr/local/include/cifx"],
            libraries = ["cifx", "rt", "pthread", "pciaccess"],
            library_dirs = ["/usr/local/lib"],
            extra_link_args = [],
        )

        ffi.compile()

    print("invoke : Done !")

@invoke.task()
def build_cffi_cifx(c):
    """
    Build the CFFI Hilscher cifX Python bindings.
    """

    print_banner("Building CFFI Hilscher cifX Module")

    ffi = cffi.FFI()

    this_dir = pathlib.Path().resolve()
    h_file_name = this_dir / "cifXUser_cffi.h"

    with open(h_file_name) as h_file:
        # cffi does not support directives yet,
        # so we need to remove them
        lns = h_file.read().splitlines()
        flt = filter(lambda ln: not re.match(r" *#", ln), lns)
        flt = map(lambda ln: ln.replace("__CIFx_PACKED_PRE ", ""), flt)
        flt = map(lambda ln: ln.replace("__CIFx_PACKED_POST ", ""), flt)
        flt = map(lambda ln: ln.replace("APIENTRY ", ""), flt)
        ffi.cdef(str("\n").join(flt), pack = 1)

        ffi.set_source(
            "cffi_cifx",
            """
            #include "cifXUser.h"
            """,
            include_dirs = ["/usr/local/include/cifx"],
            libraries = ["cifx", "rt", "pthread", "pciaccess"],
            library_dirs = ["/usr/local/lib"],
            extra_link_args = [],
        )

        ffi.compile()

    print("invoke : Done !")

@invoke.task(
#    build_cffi_cifx_linux,
#    build_cffi_cifx
)
def cifXTest(c):
    """
    Run the test script of the CFFI Python bindings.
    """

    print_banner("Running Hilscher cifX CFFI Module test")

    invoke.run("python cifXTest.py", pty = not on_win)

    print("invoke : Done !")

@invoke.task(
)
def cifX_OMB(c):
    """
    Run the Hilscher cifX to Modbus TCP Gateway.
    """

    print_banner("Running Hilscher cifX to Modbus TCP Gateway")

    invoke.run("python cifX-OMB.py", pty = not on_win)

    print("invoke : Done !")

@invoke.task(
    clean,
    build_cffi_cifx_linux,
    build_cffi_cifx,
    cifXTest,
)
def all(c):
    """
    Build and run tests.
    """

    print_banner("Running all")

    pass

