#!/usr/bin/env python3

from cffi_cifx_linux import ffi as ffilinux, lib as liblinux
from cffi_cifx import ffi, lib

def int32_to_hex(n):
    return "0x%s"%("00000000%x"%(n & 0xffffffff))[-8:]

def int16_to_hex(n):
    return "0x%s"%("0000%x"%(n & 0xffff))[-4:]

def int8_to_hex(n):
    return "0x%s"%("00%x"%(n & 0xff))[-2:]

def Linux_cifXGetDriverVersion(info):
    """
    Gets the Linux cifX Driver version and fills the info dictionnary
    """
    Result = 0

    pVersion = ffilinux.new("char[50]")
    print("sizeof(pVersion) =", ffi.sizeof(pVersion))

    Result = liblinux.cifXGetDriverVersion(ffi.sizeof(pVersion), pVersion)

    if Result != 0 :
        print("Error cifXGetDriverVersion :", int32_to_hex(Result))
    else:
        info["Linux Driver Version"] = ffi.string(pVersion,
                                                  ffi.sizeof(pVersion)).decode()
        print("Linux Driver Version :", info["Linux Driver Version"])

def Log_Error(Where, Result):
    pErrorBuffer = ffilinux.new("char[50]")

    if Result != 0 :
        lib.xDriverGetErrorDescription(Result, pErrorBuffer,
                                       ffi.sizeof(pErrorBuffer))

        print("Error", Where, ":", int32_to_hex(Result),
              ffi.string(pErrorBuffer, ffi.sizeof(pErrorBuffer)).decode())
    else :
        print(Where, ":", "No error")

def cifXGetDriverVersion(hDriver, info):
    """
    Gets the cifX Driver Toolkit version and fills the info dictionnary
    """
    Result = 0

    pDriverInfo = ffi.new("DRIVER_INFORMATION*")
    print("sizeof(pDriverInfo[0]) =", ffi.sizeof(pDriverInfo[0]))

    Result = lib.xDriverGetInformation(hDriver,
                                       ffi.sizeof(pDriverInfo[0]),
                                       pDriverInfo)

    if Result != 0 :
        Log_Error("xDriverGetInformation", Result)
    else:
        print("sizeof(pDriverInfo.abDriverVersion) =",
              ffi.sizeof(pDriverInfo.abDriverVersion))

        info["Driver Version"] = \
            ffi.string(pDriverInfo.abDriverVersion,
                       ffi.sizeof(pDriverInfo.abDriverVersion)).decode()

        info["Board count"] = pDriverInfo.ulBoardCnt

        print("xDriverGetInformation :\n",
              "Driver Version\t:", info["Driver Version"],
              "\n", "Board count\t:", info["Board count"])


